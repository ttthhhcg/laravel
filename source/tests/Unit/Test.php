<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class Test extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $a = new \App\Http\Controllers\TestController();
        $response = $a->index();
        $this->assertEquals('abcd', $response);
    }
}
